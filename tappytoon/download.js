(async function () {
  const fs = require('fs')
  const got = require('got')
  const util = require('util')

  const {promisify} = require('util');
  const stream = require('stream');
  const pipeline = promisify(stream.pipeline)

  const SKIP = parseInt(process.env.SKIP) || 0
  const COMIC_ID = process.env.COMIC_ID

  const TOKEN = process.env.TOKEN

  const API_BASE = 'https://api-global.tappytoon.com'

  let product_url = `${API_BASE}/comics/${COMIC_ID}/chapters?excludes=wait_until_free&sort=asc&locale=en`
  const options = {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Authorization': `Bearer ${TOKEN}`,
    }
  }

  let response = await got(product_url, options)
  let product = JSON.parse(response.body)

  for (const ep of product.slice(SKIP)) {
    let destination = `downloads/${ep.comicId}/${ep.order.toString().padStart(3, '0')}`
    console.log(`Creating directory ${destination}`)
    fs.mkdirSync(destination, { recursive: true })

    let episode_url = `${API_BASE}/content-delivery/contents?chapterId=${ep.id}&variant=high&locale=en`
    console.log(episode_url)

    let response = await got(episode_url, options)
    let blob = JSON.parse(response.body)

    for (const page of blob.media) {
      let paddedN = `${page.sortKey}`.padStart(3, '0')
      let filename = `${destination}/${paddedN}.jpg`

      if (await fs.existsSync(filename)) {
        console.log(`-- ${filename} already downloaded, skipping ...`)
      } else {
        console.log(page.url)
	      await pipeline(
          got.stream(`${page.url}`),
          fs.createWriteStream(filename)
        )
      }
    }
  }
})()
