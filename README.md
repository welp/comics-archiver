# comic archiver

this is a script for creating personal archives of comics

do not use this to download comics you do not own. do not share downloaded
comics. support the artists.

# usage

see the instructions in each service’s subdirectory.
