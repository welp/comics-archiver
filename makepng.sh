#!/usr/bin/env bash

shopt -s nullglob

title="${1}"

# the files have to be named such that they'll be added in the right order; if
# necessary run something like the following to 0-pad numbered filenames:
#
# for i in *; do if [[ -d ${i} ]]; then pushd ${i}; for n in $(seq 1 9); do mv ${n}.jpg 0${n}.jpg; done; popd; fi; done

for i in "${title}"/*; do
  subdir="$(basename "${i}")"
  if [[ -d "${i}" ]]; then
    pushd "${i}" || exit 1
    convert ./* -append "${subdir}-combined.png"
    popd || exit 1
  fi
done
