(async function () {
  const fs = require('fs')
  const got = require('got')
  const util = require('util')

  const SKIP = parseInt(process.env.SKIP) || 0

  const PRODUCT_JSON = process.env.PRODUCT_JSON
  const PRODUCT = JSON.parse(fs.readFileSync(PRODUCT_JSON))

  const TOKEN = process.env.TOKEN

  const comic_id = PRODUCT.product.id
  console.log(`Comic ID: ${comic_id}`)
  const comic_url_base = `https://rcdn.lezhin.com/v2/comics/${comic_id}/episodes`

  const auth_url_base = `https://www.lezhinus.com/lz-api/v2/cloudfront/signed-url/generate?contentId=${comic_id}`
  for (const ep of PRODUCT.all.slice(SKIP)) {
    let destination = `downloads/${PRODUCT.product.alias}/${ep.seq.toString().padStart(3, '0')}`
    console.log(`Creating directory ${destination}`)
    fs.mkdirSync(destination, { recursive: true })

    let episode_url_base = `${comic_url_base}/${ep.id}/contents/scrolls`

    let signing_url = `${auth_url_base}&episodeId=${ep.id}&purchased=${ep.purchased == true}&q=30&firstCheckType=P`

    console.log(signing_url)

    let options = {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${TOKEN}`,
      }
    }
    let response = await got(signing_url, options)
    let blob = JSON.parse(response.body)

    console.log(blob)

    let n = 1

    let url = `${episode_url_base}/${n}.webp?purchased=${ep.purchased == true}&q=30&updated=${blob.data.now}&Policy=${blob.data.Policy}&Signature=${blob.data.Signature}&Key-Pair-Id=${blob.data['Key-Pair-Id']}`
    try {
      while (await got.head(url)) {
        console.log(`-- 200 -- ${url}`)

        let paddedN = `${n}`.padStart(3, '0')
        let filename = `${destination}/${paddedN}.webp`

        if (await fs.existsSync(filename)) {
          console.log(`-- ${filename} already downloaded, skipping ...`)
        } else {
          got.stream(`${url}`).pipe(fs.createWriteStream(filename))
        }

        n++
        url = `${episode_url_base}/${n}.webp?purchased=${ep.purchased == true}&q=30&updated=${blob.data.now}&Policy=${blob.data.Policy}&Signature=${blob.data.Signature}&Key-Pair-Id=${blob.data['Key-Pair-Id']}`
      }
    } catch (err) {
      console.dir(err)

      console.log(`-- 404 -- ${url}`)
      console.log('--- Hit 404, bailing out.')
    }

    // await new Promise(res => setTimeout(res, 30000))
  }
})()
