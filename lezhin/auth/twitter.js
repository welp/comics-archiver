const fs = require('fs')
const puppeteer = require('puppeteer')

const LOGIN_PAGE = 'https://www.lezhin.com/en/login'
const USER = process.env.LEZHIN_USER
const PASS = process.env.LEZHIN_PASS

if (!USER) {
  console.log('--- ERROR: env variable LEZHIN_USER unset.')
  process.exit(1)
}

if (!PASS) {
  console.log('--- ERROR: env variable LEZHIN_PASS unset.')
  process.exit(1)
}

puppeteer.launch().then(async browser => {
  const page = await browser.newPage()
  await page.goto(LOGIN_PAGE)

  // https://github.com/GoogleChrome/puppeteer/issues/3347#issuecomment-427234299
  await page.evaluate(() => {
    return document.querySelector(`button[data-service="twitter"]`).click()
  })
  await page.waitForNavigation()

  // this is twitter-specific
  const userField = await page.$('#username_or_email')
  const passField = await page.$('#password')

  await userField.type(USER)
  await passField.type(PASS)

  await page.click('#allow')
  await page.waitForNavigation()

  const cookies = await page.cookies()
  fs.writeFileSync('cookies.json', JSON.stringify(cookies))

  await page.close()
  await browser.close()
})
