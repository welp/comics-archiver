# lezhin

log in and go to the series page for the comic you want to archive.  pop open
the web inspector and grab your `token` from `__LZ_CONFIG__`.  Save
`__LZ_PRODUCT__` in its entirey as a json file.

```
TOKEN=<token> PRODUCT_JSON=path/to/product.json node download.js
```

to combine into a pdf, install poppler and run something like this:
```
for dir in *; pushd $dir; for img in *.webp; convert $img (basename $img webp)pdf; end; pdfunite *.pdf ../$dir-combined.pdf; popd; end
```
